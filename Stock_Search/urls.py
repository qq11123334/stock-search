from django.urls import path
from django.contrib.staticfiles.views import serve
 
from . import form, draw
urlpatterns = [
    path('', form.search_form),
    path('search/', form.show_data),
    path('search-form/', form.search_form),
    path('search/plot.jpg/', draw.get_mat),
    path('search/learn.jpg/', draw.get_learn),
    path('favicon.ico/', serve, {'path' : 'favicon.ico'}),
]