from django.http import HttpResponse
from django.shortcuts import render

stock = ["0050", "0051", "2330", "2409", "2454", "2498", "2885", "2886", "2890", "2891"]
stock_name = { 
    "0050" : "元大台灣50",
    "0051" : "中型100", 
    "2330" : "台積電",
    "2409" : "友達",
    "2454" : "聯發科",
    "2498" : "宏達電",
    "2885" : "元大金",
    "2886" : "兆豐金",
    "2890" : "永豐金",
    "2891" : "中信金"
}
def cal_dif(a, b):
    cnt = 0
    a_len = len(a)
    cnt = cnt + (4 - a_len)
    for i in range(min(a_len, 4)):
        if a[i] != b[i]:
            cnt = cnt + 1
    return cnt

def predict_want(content):
    cur = ""
    cur_mx = 2
    for stock_number in stock:
        res = cal_dif(content, stock_number)
        if res <= cur_mx:
            cur_mx = res
            cur = stock_number
    return cur

# 送出表單
def search_form(request):
    return render(request, 'input_form.html', {"stock" : stock})

# 接收表單並回傳結果
def show_data(request):
    request.encoding='utf-8'
    content = request.GET['q']
    if 'q' in request.GET and request.GET['q'] and content.isdigit() and content in stock:
        response_dic = {
        "stock_number" : content,
        "stock_name" : stock_name[content],
        "img1_src" : "plot.jpg/?q=" + content,
        "img2_src" : "learn.jpg/?q=" + content
        }
        return render(request, 'show_stock.html', response_dic)    
    else:
        predict_want_number = predict_want(content)
        if predict_want_number != "":
            str_want_stock_number = "你是不是想搜尋" + predict_want_number
        else:
            str_want_stock_number = ""

        response_dic = {
            "content" : content, 
            "str_want_stock_number" : str_want_stock_number
        }
        return render(request, 'error.html', response_dic)