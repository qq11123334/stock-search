import numpy as np
import matplotlib.pyplot as plt
import csv
import numpy as np
from django.http import HttpResponse
from django.shortcuts import render
from matplotlib.figure import Figure
from matplotlib.backends.backend_agg import FigureCanvasAgg
from matplotlib.dates import DateFormatter
import matplotlib.ticker as ticker

def open_csv(filename):
    with open(filename, newline="") as csvfile:
        rows = csv.reader(csvfile)
        rows = list(rows)
        rows = np.transpose(rows).tolist()

    return rows

def get_mat(request):
    stock_number = request.GET['q']
    data = open_csv('./templates/20d/' + stock_number + '_20d.csv')
    data[0][0] = data[0][1]
    data[1][0] = data[1][1]
    data[2][0] = data[2][1]
    for i in range(len(data[2])):
        data[2][i] = float(data[2][i])

    for i in range(len(data[1])):
        data[1][i] = float(data[1][i])

    fig = Figure()
    ax = fig.add_subplot(1, 1, 1)
    # ax.plot(price, '-')

    data1_len = len(data[1]) - 1
    ax.plot(data[0][:data1_len], data[1][:data1_len])
    
    ax.plot(data[0], data[2])
    ax.set_xlabel('Date')
    ax.set_ylabel('Price')
    ax.set_title(request.GET['q'] + " Prediction in the future")
    ax.xaxis.set_major_locator(ticker.MultipleLocator(base=250))
    
    canvas = FigureCanvasAgg(fig)
    response = HttpResponse(content_type='image/jpg')
    canvas.print_png(response)
    return response

def get_learn(request):
    stock_number = request.GET['q']
    data = open_csv('./templates/5y/' + stock_number + '_5y.csv')
    data[0][0] = data[0][1]
    data[1][0] = data[1][1]
    data[2][0] = data[2][1]
    for i in range(len(data[2])):
        data[2][i] = float(data[2][i])

    for i in range(len(data[1])):
        data[1][i] = float(data[1][i])

    fig = Figure()
    ax = fig.add_subplot(1, 1, 1)
    # ax.plot(price, '-')
    ax.plot(data[0], data[1])
    ax.plot(data[0], data[2])
    ax.set_xlabel('Date')
    ax.set_ylabel('Price')
    ax.set_title(request.GET['q'] + " Prediction vs Reality in the past")
    ax.xaxis.set_major_locator(ticker.MultipleLocator(base=250))
    
    canvas = FigureCanvasAgg(fig)
    response = HttpResponse(content_type='image/jpg')
    canvas.print_png(response)
    return response